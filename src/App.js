import React from 'react';
import styled, {ThemeProvider, createGlobalStyle} from 'styled-components/macro';
import Header from './Header/Header';
import themes from './Themes';

const GlobalStyle = createGlobalStyle`
* {
-webkit-box-sizing: border-box;
   -moz-box-sizing: border-box;
        box-sizing: border-box;
}

body{
  margin: 0;
  padding: 0;
}
`;

function App() {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={themes}>
        <div className="App">
          <Header />
        </div>
      </ThemeProvider>
    </>
  );
}

export default App;
