import React from 'react';
import styled from 'styled-components/macro';

const Wrapper = styled.div`
    font-family: 'Open Sans', sans-serif;
    width: 100%;
    display: flex;
    justify-content: flex-end;
    margin-right: 50px;
    height: 50px;
`;

const MenuWrapper = styled.div`
    width: 35%;
    right: 30px;
    display: flex;
    justify-content: space-around;
    align-items: center;
`;


const Button = styled.button`
    color: ${props => props.theme.black};
    font-size: 1em;
    background-color: transparent;
    border-style: none;
`;

const InlineSeparator = styled.span`
    width: 1px;
    height: 21px;
    position: relative;
    
    :before{
        position: absolute;
        content: '';
        width: 1px;
        height: 31px;
        top: -5px;
        left: 0;
        background-color: #b9b9b9;
    }

`;

const Menu = () =>{
    return(
        <Wrapper>
            <MenuWrapper>
                <Button>Logowanie</Button>
                <InlineSeparator />
                <Button>Rejestracja</Button>
                <InlineSeparator />
                <Button>Pomoc</Button>
                <InlineSeparator />
                <Button>Kontakt</Button>
            </MenuWrapper>
        </Wrapper>
    );
}

export default Menu;