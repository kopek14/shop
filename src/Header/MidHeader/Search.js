import React from 'react';
import styled from 'styled-components/macro';
import './fontello/css/icons.css';

const Form = styled.form`
    flex: 6;
`;

const Wrapper = styled.div`
    display: flex;
`;

const Button = styled.button`
    font-size: 1em;
    background-color: transparent;
    border: 2px ${({theme}) => theme.green } solid;
    flex: 1;
`;

const Input = styled.input`
    border: 2px solid ${props => props.theme.green};
    border-right: none;
    flex: 10;
    height: 50px;
`;
const Icon = styled.i`
    color: ${props => props.theme.green};
    font-size: 30px;
`;




const Search = () =>{
    return(
        <Form>
            <Wrapper>
                <Input type="text"></Input>
                <Button><Icon className="icon-search-outline"></Icon></Button>
            </Wrapper>
        </Form>
    )
}

export default Search;