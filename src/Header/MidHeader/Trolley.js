import React from 'react';
import styled from 'styled-components/macro';
import './fontello/css/icons.css';

const Icon = styled.i`
    color: ${props => props.theme.green};
    font-size: 50px;
    flex: 2;
    text-align: center;
`;

const Trolley = () => {
    return(
        <Icon className="icon-basket"></Icon>
    )
}

export default Trolley;