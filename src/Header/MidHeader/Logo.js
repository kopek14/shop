import React from 'react';
import styled from 'styled-components/macro';

const H1Logo = styled.h1`
    color: ${props => props.theme.green};
    flex: 3;
    margin: 0;
    text-align: center;
`;

const Logo = () =>{
    return(
        <H1Logo>Logo sklepu internetowego</H1Logo>
    )
}

export default Logo;