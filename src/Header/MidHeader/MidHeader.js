import React from 'react';
import styled from 'styled-components/macro';
import Logo from './Logo';
import Search from './Search';
import Trolley from './Trolley';

const Wrapper = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: center;
`;



const MidHeader = () => {
    return(
        <Wrapper>
            <Logo />
            <Search />
            <Trolley />
        </Wrapper>
    )
}

export default MidHeader;