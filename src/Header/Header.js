import React from 'react';
import styled from 'styled-components/macro';
import NavBar from './NavBar/NavBar';
import Menu from './Menu/Menu';
import MidHeader from './MidHeader/MidHeader';


const Wrapper = styled.header`
    box-shadow: 0px 3px 4px 0px rgba(0,0,0,0.55);
`;

const Header = ()=>{
    return(
        <Wrapper>
            <Menu />
            <MidHeader />
            <NavBar />
        </Wrapper>
    )
}

export default Header;