import React from 'react';
import styled from 'styled-components/macro';

const transition = 0.5;

const Nav = styled.nav`
    display: flex;
    justify-content: center;
`;

const Ul = styled.ul`
    width: 35%;
    font-size: 16px;
    font-weight: bold;
    color: ${({theme}) => theme.green};
    list-style: none;
    display: flex;
    justify-content: space-between;
`;

const Li = styled.li`
    position: relative;
    cursor: pointer;
    padding: 5px;
    transition: ${transition}s;

    :hover{
        background-color: ${({theme}) => theme.green};
        color: white;
    }
`;

const UlVert = styled.ul`
    list-style: none;
    display: none;
    padding: 10px 0 0 0;
    position: absolute;
    left: 0px;
    
    ${Li}:hover &{
        display: block;
    }

    :last-child{
        box-shadow: 0px 3px 4px 0px rgba(0,0,0,0.55);
    }
`;

const LiVert = styled.li`
    color: ${({theme}) => theme.green};
    background-color: #f2f2f2;
    padding: 15px;
    width: 100px;

    :hover{
        background-color: ${({theme}) => theme.green};
        color: white;
        
    }
`

const Triangle = styled.div`
    display: inline-block;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 5px 0 4px 7px;
    border-color: transparent transparent transparent ${({theme}) => theme.green};
    transition: ${transition}s;

    ${Li}:hover &{
        border-color: transparent transparent transparent white;
        transform: rotate(90deg);
    }

`;

const NavBar = () =>{
    return(
            <Nav>
                <Ul>
                    <Li>Odzież <Triangle />
                            <UlVert>
                                <LiVert>Bluzy</LiVert>
                                <LiVert>Spodnie</LiVert>
                                <LiVert>Czapki</LiVert>
                            </UlVert>
                    </Li> 
                    <Li>Buty <Triangle />
                            <UlVert>
                                <LiVert>Sportowe</LiVert>
                                <LiVert>Zimowe</LiVert>
                                <LiVert>Sandały</LiVert>
                            </UlVert>
                    </Li>
                    <Li>Akcesoria <Triangle />
                            <UlVert>
                                <LiVert>Zegarki</LiVert>
                                <LiVert>Gadżety</LiVert>
                                <LiVert>Naszyjniki</LiVert>
                            </UlVert>
                    </Li>
                    <Li>Więcej <Triangle />
                            <UlVert>
                                <LiVert>Coś</LiVert>
                                <LiVert>Wymyślę</LiVert>
                                <LiVert>Później</LiVert>
                            </UlVert>
                    </Li>
                </Ul>
            </Nav>
    );
};

export default NavBar;